# LappaORM

[![Join the chat at https://gitter.im/Arctium-Software/LappaORM](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/Arctium-Software/LappaORM?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

### What is Lappa ORM?
Lappa ORM is a object relational mapper for .NET Core (MySQL/MSSQL) written in C# released under the MIT License.

###.NET compatibility:
- .NET Core (netstandard1.6)

###Supported .NET Core compatible MySQL connectors:
- https://github.com/SapientGuardian/mysql-connector-net-netstandard (A fork of the official MySQL Connector, GPL licensed)
- https://github.com/bgrainger/MySqlConnector (A fresh, clean & fully async ADO.NET MySQL connector, MIT licensed)
