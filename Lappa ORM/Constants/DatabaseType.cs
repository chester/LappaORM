﻿// Copyright (C) Arctium Software.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace LappaORM.Constants
{
    public enum DatabaseType
    {
        MySql  = 0,
        MSSql  = 1,
        SQLite = 2
    }
}
